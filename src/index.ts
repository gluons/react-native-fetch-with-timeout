/**
 * Abort handler.
 * Prevent error thrown when `fetch` aborted.
 *
 * @param err Error
 * @returns Empty JSON response (`{}`)
 */
export const AbortHandler = (err: Error) => {
	if (err?.name === 'AbortError') {
		return new Response('{}');
	}

	throw err;
};

/**
 * Options of `fetchWithTimeout`.
 */
export interface FetchWithTimeoutOptions {
	/**
	 * Timeout (ms) (`false` for disabling timeout. Use plain `fetch`.)
	 *
	 * @default false
	 */
	timeout?: number | false;
	/**
	 * Abort controller
	 *
	 * @default new AbortController()
	 */
	abortController?: AbortController;
	/**
	 * Handler when `fetch` aborted.
	 *
	 * @default AbortHandler
	 */
	abortHandler?: typeof AbortHandler;
}

/**
 * `fetch` with timeout support.
 *
 * @param {RequestInfo} input Request info
 * @param {RequestInit} init Request options
 * @param {FetchWithTimeoutOptions} timeout `fetchWithTimeout`'s options
 */
export default async function fetchWithTimeout(
	input: RequestInfo,
	init?: RequestInit,
	options: FetchWithTimeoutOptions = {
		timeout: false,
		abortController: new AbortController(),
		abortHandler: AbortHandler
	}
): Promise<Response> {
	const {
		timeout = false,
		abortController = new AbortController(),
		abortHandler = AbortHandler
	} = options;

	init = {
		...init,
		signal: abortController.signal
	};

	if (!timeout) {
		return fetch(input, init).catch(abortHandler);
	}

	return Promise.race([
		fetch(input, init).catch(abortHandler),
		new Promise<Response>((_, reject) => {
			setTimeout(() => {
				abortController.abort();

				reject(new Error('Request Timeout'));
			}, timeout);
		})
	]);
}
