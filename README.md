# React Native Fetch with Timeout

> Personal usage only. No further support.

[Fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) with timeout for [React Native](https://reactnative.dev/).

## Installation

```bash
npm install -S @gluons/react-native-fetch-with-timeout
```

or

```bash
yarn add @gluons/react-native-fetch-with-timeout
```

## Usage

```js
import { Alert } from 'react-native';
import fetchWithTimeout from '@gluons/react-native-fetch-with-timeout';

(async () => {
	try {
		const res = await fetchWithTimeout(
			'https://<request-url>',
			{},
			{
				timeout: 5000 // 5 seconds
			}
		)
	} catch (err) {
		// Thrown "Request Timeout" error after 5 seconds
		Alert.alert('Error', err.toString());
	}
})();
```
